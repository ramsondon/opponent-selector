import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import DemoQuickMatchSingle from "@/components/DemoQuickMatchSingle";
import Demo1vs1 from "@/components/Demo1vs1";
import DemoHome from "@/components/DemoHome";

Vue.config.productionTip = false;
Vue.config.performance = true;
Vue.use(VueRouter);

const routes = [
  { path: '/', component: DemoHome },
  { path: '/demo/quickmatch/single', component: DemoQuickMatchSingle },
  { path: '/demo/quickmatch/double', component: Demo1vs1 }
];

const router = new VueRouter({
  routes // short for `routes: routes`
});


new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
