
export default class Player {
    constructor(id, name, image) {
        this.id = id;
        this.name = name;
        this.image = image;
    }
}